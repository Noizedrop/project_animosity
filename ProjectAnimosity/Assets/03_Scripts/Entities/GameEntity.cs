﻿using UnityEngine;
using System;
using System.Collections.Generic;
using Behaviors;

namespace Entities
{
    /// <summary>
    /// A simple class to describe an game entity. The entity behaves in a way described by a list of behaviors.
    /// </summary>
    public class GameEntity : MonoBehaviour
    {
        /// <summary>
        /// List of behaviors that is used to describe the entity.
        /// </summary>
        public List<AbstractBehaviour> behaviorsList = new List<AbstractBehaviour>();

        private Dictionary<Type, AbstractBehaviour> behavioursMap = new Dictionary<Type, AbstractBehaviour>();

        private void Start()
        {
            InitilizeBehaviors();
        }

        protected virtual void InitilizeBehaviors()
        {
            AbstractBehaviour behavior;            

            int count = behaviorsList.Count; //look up if this is necessary. It is not Flash.
            for (int i = 0; i < count; i++)
            {
                behavior = behaviorsList[i];
                behavior.Init(this);
                behavior.Activate();                
                behavioursMap.Add(behavior.GetType(), behavior);
            }          
        }        

        // Maybe better to create a parent that will take care of update in single separate function (think about this..)
        void Update()
        {
            AbstractBehaviour behavior;
            int count = behaviorsList.Count; //look up if this is necessary. It is not Flash.
            for (int i = 0; i < count; i++)
            {
                behavior = behaviorsList[i];
                if (behavior.Active) behavior.BehaviorUpdate(this);
            }
        }

        public AbstractBehaviour GetBehaviour<T>()
        {
            return behavioursMap[typeof(T)];
        }
        
        public bool IsBehaviourActive<T>()
        {
            Type behaviorType = typeof(T);

            if (behavioursMap.ContainsKey(behaviorType)) return false;

            return behavioursMap[behaviorType].Active;
        }        
    }
}
