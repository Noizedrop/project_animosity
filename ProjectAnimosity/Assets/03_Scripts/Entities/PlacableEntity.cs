﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Behaviors;

namespace Entities
{
    /// <summary>
    /// Item that can be placed on a game scene.
    /// </summary>
    public class PlacableEntity : GameEntity
    {
        private bool placable = true;
        private Renderer rend;
        private RotateItemBehaviour rotateItemBehaviour;

        //TODO - redo
        /// <summary>        
        /// A flag that defines is the entity is placeable on the scene.
        /// </summary>
        public bool Placable
        {
            get => placable;
            set
            {
                if (placable == value) return;

                placable = value;

                Color newColor = placable ? Color.green : Color.red;

                ChangeItemColor(newColor);
            }
        }

        /// <summary>
        /// If True the <b>RotateItemBehaviour</b> is active and the user is currently rotating the item.
        /// </summary>
        public bool Rotating
        {
            get => rotateItemBehaviour.Rotating;
        }

        void Awake()
        {
            rend = GetComponent<Renderer>();
        }

        override protected void InitilizeBehaviors()
        {
            base.InitilizeBehaviors();

            rotateItemBehaviour = GetBehaviour<RotateItemBehaviour>() as RotateItemBehaviour;
        }

        /// <summary>
        /// New color setter.
        /// </summary>
        /// <param name="newColor">A new color value.</param>
        protected void ChangeItemColor(Color newColor)
        {
            rend.material.SetColor("_BaseColor", newColor);
        }
    }
}
