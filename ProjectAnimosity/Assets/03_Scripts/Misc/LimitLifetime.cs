﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LimitLifetime : MonoBehaviour 
{
	public float lifeTime = 3.0f;
    public Transform dissolve_Prefab;

	void Start()
	{
		Invoke("Die",lifeTime);
	}

	void Die()
	{
        if (dissolve_Prefab)
            Instantiate(dissolve_Prefab, transform.position, transform.rotation);
		Destroy(this.gameObject);
	}
}
