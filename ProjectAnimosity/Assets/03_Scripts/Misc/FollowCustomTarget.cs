﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCustomTarget : MonoBehaviour
{
    public Transform target;
    public Vector3 offset;

    // Use this for initialization
    void Start()
    {
        if (target == null)
            target = GameObject.FindGameObjectWithTag("Player").transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (target != null)
            transform.position = target.position + offset;
        else
            Destroy(this.gameObject);
    }
}
