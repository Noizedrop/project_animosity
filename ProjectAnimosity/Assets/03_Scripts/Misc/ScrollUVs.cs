﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollUVs : MonoBehaviour
{
    private TrailRenderer line;
    public float scrollSpeed;

    private void Start()
    {
        line = GetComponent<TrailRenderer>();
    }

    void Update()
    {
        line.material.mainTextureOffset = new Vector2(Time.time * scrollSpeed, 0f);
    }
}
