﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class UIRessources : MonoBehaviour
{
    public Transform PallasitPanel;
    public Image PallasitBar;
    public Text PallasitText;
    public Gradient PallasitGradient;

    public Transform IyotidPanel;
    public Image IyotidBar;
    public Text IyotidText;
    public Gradient IyotidGradient;

    public Transform BioniumPanel;
    public Image BioniumBar;
    public Text BioniumText;
    public Gradient BioniumGradient;



    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    public void UpdateUI()
    {
        //Pallasit
        PallasitText.text = "" + PlayerRessources.instance.currentPallasit + " / " + PlayerRessources.instance.maxPallasit;
        float PallasitFraction = (float)PlayerRessources.instance.currentPallasit / (float)PlayerRessources.instance.maxPallasit;
        PallasitBar.fillAmount = PallasitFraction;
        PallasitBar.color = PallasitGradient.Evaluate(PallasitFraction);

        //Iyotid 
        IyotidText.text = "" + PlayerRessources.instance.currentIyotid + " / " + PlayerRessources.instance.maxIyotid;
        float IyotidFraction = (float)PlayerRessources.instance.currentIyotid / (float)PlayerRessources.instance.maxIyotid;
        IyotidBar.fillAmount = IyotidFraction;
        IyotidBar.color = IyotidGradient.Evaluate(IyotidFraction);

        //Bionium
        BioniumText.text = "" + PlayerRessources.instance.currentBionium + " / " + PlayerRessources.instance.maxBionium;
        float BioniumFraction = (float)PlayerRessources.instance.currentBionium / (float)PlayerRessources.instance.maxBionium;
        BioniumBar.fillAmount = BioniumFraction;
        BioniumBar.color = BioniumGradient.Evaluate(BioniumFraction);
    }
}
