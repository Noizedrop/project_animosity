﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiningFacility : MonoBehaviour
{
    //later this has to track the ore vein it is build upon
    //public OreVein vein;

    public float productionInterval;
    private float currentCycle;
    public int productionAmount;

    //for energy system
    public bool isPowered;
    public bool energyConsumption;

    // Start is called before the first frame update
    void Start()
    {
        currentCycle = Time.time + productionInterval;
    }

    // Update is called once per frame
    void Update()
    {
        if(Time.time > currentCycle)
        {
            Debug.Log("test");
            //check if the vein still has ressources left

            PlayerRessources.instance.AddPallasit(productionAmount);
            currentCycle = Time.time + productionInterval;
        }
    }
}
