﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Stores the Ressources of a single Player
public class PlayerRessources : SingletonBehaviour<PlayerRessources>
{
    public int playerID;

    [Header("Minerals")]
    public int maxPallasit;                 //cumulated Maximum amount of Pallasit that is storable. Used as Building Ressource
    public int currentPallasit;             //current amount of Pallasit distributed in all storages

    public int maxBionium;                  //cumulated Maximum amount of Bionium that is storable. Used as Fuel for Units
    public int currentBionium;              //current amount of Bionium distributed in all storages

    public int maxIyotid;                 //cumulated Maximum amount of Iyotid that is storable. Used by PowerPlants to generate Energy
    public int currentIyotid;              //current amount of Iyotid distributed in all storages

    //[Header("Buildings")]


    //[Header("Units")]

    private void Start()
    {
        CollectBuildings();


    }

    //used to gather preplaced Buildings for certain level structures
    private void CollectBuildings()
    {
        //Iterate through scene and collect all buildings in it, if they are assigned to the player.
        //Add to the list
    }

    public void AddPallasit(int value)
    {
        Debug.Log(value);
        currentPallasit += value;
        currentPallasit = Mathf.Clamp(currentPallasit, 0, maxPallasit);     //clamp instead if to prevent ressource loss

        UIController.instance.Ressources.UpdateUI();
    }
}
