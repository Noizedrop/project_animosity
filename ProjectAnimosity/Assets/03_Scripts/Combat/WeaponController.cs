﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour
{
    public float nextShot = 0f;
    public float cooldown;
    public float accuracy;

    //public int Reload = 0;
    //public float ReloadDuration = 0f;

    public Transform shotSpawn;
    //public Transform muzzleFlash;
    public Transform projectile;

    public void Trigger()
    {
        if(Time.time >= nextShot)
        {
            Quaternion randomRot = Quaternion.Euler(new Vector3(Random.Range(-accuracy,accuracy), Random.Range(-accuracy, accuracy),0f));
            Projectile newProj = Instantiate(projectile, shotSpawn.position, shotSpawn.rotation * randomRot).GetComponent<Projectile>();
            newProj.owner = this.transform;
            nextShot = Time.time + cooldown;
        }
    }
}
