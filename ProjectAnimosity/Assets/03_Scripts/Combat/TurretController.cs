﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretController : MonoBehaviour
{
    //Can be used on all Turrets
    //Finds targets and tracks them
    private WeaponController weapon;

    public float rotationSpeed;
    public float clampRotation = 20f;
    public float threshold = 0.95f;
    private float dotProduct;

    public Transform target = null;


    // Start is called before the first frame update
    void Start()
    {
        if (!weapon)
            weapon = GetComponent<WeaponController>();
    }

    // Update is called once per frame
    void Update()
    {
        if(target)
        {
            RotateToTarget();
            if(dotProduct >= threshold)
            {
                weapon.Trigger();
            }
        }
    }

    private void RotateToTarget()
    {
        Vector3 targetDirection = target.position - transform.position;
        dotProduct = Vector3.Dot(targetDirection.normalized, transform.forward);
        float step = rotationSpeed * Time.deltaTime;

        Vector3 newDirection = Vector3.RotateTowards(transform.forward, targetDirection, step, 0.0f);
        Debug.DrawRay(transform.position, newDirection, Color.red);
        transform.rotation = Quaternion.LookRotation(newDirection);
        //transform.eulerAngles.x = Mathf.Clamp(transform.eulerAngles.x, -90 - clampRotation, -90 + clampRotation);
    }

    public void OnTriggerEnter(Collider other)
    {
        Debug.Log("Triggered");
        if(other.tag == "EnemyUnit" && target == null)
        {
            AssignTarget(other.gameObject.transform);
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if(other.tag == "EnemyUnit" &&  other.gameObject.transform == target)
        {
            LooseTarget(); 
        }
    }

    private void AssignTarget(Transform newTarget)
    {
        target = newTarget;
    }

    private void LooseTarget()
    {
        target = null;
    }
}
