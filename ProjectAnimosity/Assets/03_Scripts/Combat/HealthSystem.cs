﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class HealthSystem : MonoBehaviour
{
    public int maxHealth;
    public int currentHealth;

    public Transform DeathEffect;

    public UnityEvent OnDeath;
    public UnityEvent OnHit;

    public void GetHit(int damage, DamageType type)
    {
        OnHit.Invoke();

        //Do the damage type thingie

        currentHealth -= damage;
        if (currentHealth <= 0)
            Die();
    }

    public void Die()
    {
        OnDeath.Invoke();

        if (DeathEffect)
            Instantiate(DeathEffect, transform.position, transform.rotation);

        Destroy(this.gameObject);
    }
}

public enum HealthType
{
    Infantry,
    Mech,
    Glider,
    Tank,
    Building,
    Turret
}
