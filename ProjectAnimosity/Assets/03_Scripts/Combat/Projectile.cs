﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public float speed;

    public int damage;
    public DamageType type;

    public Transform impactPrefab;

    public Transform owner;
    private Rigidbody rb;
    private Vector3 oldPosition;
    

    private void Start()
    {
        
        rb = GetComponent<Rigidbody>();
        Physics.IgnoreCollision(GetComponent<Collider>(), owner.GetComponent<Collider>());
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        oldPosition = transform.position;
        rb.MovePosition(transform.position + (transform.forward * speed * Time.deltaTime));
    }

    private void OnTriggerEnter(Collider other)
    {
        RaycastHit hit;
        if(Physics.Linecast(oldPosition, transform.position, out hit));
        {
            Debug.Log(oldPosition + " " + hit.point + " " + transform.position);
            transform.position = hit.point;
        }
        if(other.tag == "EnemyUnit")
        {
            HealthSystem hs = other.GetComponent<HealthSystem>();
            if(hs != null)
            {
                hs.GetHit(damage,type); 
            }
        }
        Instantiate(impactPrefab, transform.position, transform.rotation);
        Destroy(this.gameObject);
    }
}

public enum DamageType
{
    MG,
    Cannon,
    Laser,
    Plasma,
    Rocket,
    Grenade,
    Orbital
}
