﻿using Entities;
using UnityEngine;

namespace Behaviors
{
    /// <summary>
    /// Abstract behavior that moves and places a item over the scene terrain. If the item is not placeable on the terrain fragment then it changes the item's material color.
    /// </summary>
    public abstract class AbstractPlaceItemBehaviour : AbstractBehaviour
    {
        /// <summary>
        /// The name of the layer the item can be placed on.
        /// </summary>
        [Tooltip("The name of the layer the item can be placed on.")]
        public string layerName = null;

        /// <summary>
        /// Number of samples of the placeable area. The area around the center point is sampled and checked if placeable. For just checking the borders set the value to 1.
        /// </summary>
        [Tooltip("Number of samples of the placeable area.\nThe area around the center point is sampled and checked if placeable. For just checking the borders set the value to 1.")]
        public int samplesCount = 1;

        /// <summary>
        /// Minimal allowed terrain slope vale. Every terrain sample with a surface normal steeper than this value is marked as not placeable.
        /// </summary>
        [Range(0f, 1f)]
        [Tooltip("Minimal allowed terrain slope vale. Every terrain sample with a surface normal steeper than this value is marked as not placeable.")]
        public float minTerrainSlope = 0.8f;

        /// <summary>
        /// Maximal absolute horizontal (Y) difference between center point and sample point. Used for more uniform surface levels.
        /// </summary>
        [Tooltip("Maximal absolute horizontal (Y) difference between center point and sample point.")]
        public float maxHorizontalDifference = 0.15f;

        /// <summary>
        /// Is item placed on the scene.
        /// </summary>
        public bool placedDown = false;

        /// <summary>
        /// Is item currently allowed to be placed on scene.
        /// </summary>
        protected bool placeable = false;

        /// <summary>
        /// Layer ID integer representation.
        /// </summary>
        protected int layerIndex;

        /// <summary>
        /// Direction of the ray that is cast from the sample point. Usually just straight down.
        /// </summary>
        protected Vector3 sampleRayDirection;

        /// <summary>
        /// The entity owner cast to <b>Entity.PlacableEntity</b> type.        
        /// </summary>
        protected PlacableEntity owner;

        override public void Init(GameEntity entityOwner)
        {
            layerIndex = 1 << LayerMask.NameToLayer(layerName);

            sampleRayDirection = transform.up * -1;

            owner = entityOwner as PlacableEntity;
        }

        override public void BehaviorUpdate(GameEntity item)
        {
            /*if (placeable && Input.GetMouseButtonUp(0))
            {
                placedDown = true;
                Deactivate();
            }
            else
            {*/
            Ray centerRay = Camera.main.ScreenPointToRay(Input.mousePosition);
            bool centerRayCast = Physics.Raycast(centerRay, out RaycastHit centerHit, Mathf.Infinity, layerIndex);            

            if (centerRayCast)
            {
                Vector3 centerHitPoint = owner.Rotating ? item.transform.position : centerHit.point;

                if(!owner.Rotating) placeable = IsSampleHitPointPlacable(centerHit, centerHitPoint.y);

                Vector4 sampleResult = SampleBorderAroundCenterPoint(centerHitPoint);
                placeable &= sampleResult.w != 0f;

                sampleResult = SamplePointsBetweenBorderAndCenter(sampleResult);
                placeable &= sampleResult.w != 0f;

                item.transform.position = sampleResult;
            }
            //}

            owner.Placable = placeable;
        }

        /// <summary>
        /// Abstract method that samples the terrain points on the border. Defined in derived class.
        /// </summary>
        /// <param name="centerPoint">Items center point around which border is checked.</param>
        /// <returns>A Vecto4 value that contains the center point (XYZ) with the highest Y value on the border and placeable flag (W).</returns>
        protected abstract Vector4 SampleBorderAroundCenterPoint(Vector3 centerPoint);

        /// <summary>
        /// Abstract method that checks if the terrain points between the item's center and border are placeable. Defined in derived class.
        /// </summary>
        /// <param name="centerHitPoint">Item's center point.</param>
        /// <returns><b>True</b> if the point is placeable else <b>False</b>.</returns>
        protected abstract Vector4 SamplePointsBetweenBorderAndCenter(Vector3 centerHitPoint);

        /// <summary>
        /// Method checks the normal value of the sampled terrain ray-cast hit point and the absolute vertical difference between item's 
        /// center point and the current sampled terrain point. Defined in derived class.
        /// </summary>
        /// <param name="sampleRaycastHit">Terrain sample ray cast hit.</param>
        /// <param name="centerY">The value of the item's center Y value.</param>
        /// <returns><b>True</b> if the point is placeable else <b>False</b>.</returns>        
        protected bool IsSampleHitPointPlacable(RaycastHit sampleRaycastHit, float centerY)
        {
            float horizontalDifference = Mathf.Abs(sampleRaycastHit.point.y - centerY);
            if (horizontalDifference > maxHorizontalDifference)
            {
                return false;
            }

            float terrainSlope = Mathf.Abs(Vector3.Dot(sampleRaycastHit.normal, transform.up));

            if (terrainSlope <= minTerrainSlope)
            {
                return false;
            }

            return true;
        }
    }
}