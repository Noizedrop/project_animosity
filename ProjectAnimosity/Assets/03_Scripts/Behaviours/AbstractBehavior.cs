﻿using Entities;
using UnityEngine;

namespace Behaviors
{
    /// <summary>
    /// Abstract class that is used to describe a entity's behavior. Updated by <b>GameEntity</b> owner.
    /// </summary>
    public abstract class AbstractBehaviour : MonoBehaviour
    {
        /// <summary>
        /// Class numeric hash code id. Used by <b>Dictionary</b> to differentiate between behaviors.
        /// </summary>
        public int AbstractBehaviourID { get; set; }

        /// <summary>
        /// Name identifier string of the behavior. Set by the derived classes.
        /// </summary>
        [Tooltip("Name identifier string of the behavior.")]
        public static string NAME = null;

        /// <summary>
        /// Debugging flag used during development phase. If true it will radically affect (lower) performance.
        /// </summary>
        [Tooltip("Debugging flag used during development phase. If true it will radically affect (lower) performance.")]
        public bool DEBUG_IS_DEV = true;

        private bool active = true;

        /// <summary>
        /// Flag that decides if the behavior should be updated and or affect the <b>GameEntity</b> owner.
        /// </summary>
        public bool Active { get => active; }

        /// <summary>
        /// Activate behavior.
        /// </summary>
        public void Activate()
        {
            active = true;
        }

        /// <summary>
        /// De-activate behaviors.
        /// </summary>
        public void Deactivate()
        {
            active = false;
        }

        /// <summary>
        /// Class numeric hash code id getter. Used by <b>Dictionary</b> to differentiate between behaviors.
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return AbstractBehaviourID;
        }

        /// <summary>
        /// Called by the <b>Dictionary</b> getter when comparing object hash codes.
        /// </summary>
        /// <param name="obj">Object that is compared against.</param>
        /// <returns><b>True</b> if hash codes equal else <b>False</b>.</returns>
        public override bool Equals(object obj)
        {
            return Equals(obj as AbstractBehaviour);
        }

        // <summary>
        /// Called by the <b>Dictionary</b> getter when comparing object hash codes with another <b>AbstractBehaviour</b> object.
        /// </summary>
        /// <param name="obj"><b>AbstractBehaviour</b> object that is compared against.</param>
        /// <returns><b>True</b> if hash codes equal else <b>False</b>.</returns>
        public bool Equals(AbstractBehaviour obj)
        {
            return obj != null && obj.AbstractBehaviourID == this.AbstractBehaviourID;
        }

        /// <summary>
        /// Abstract method called by owner <b>Start</b> method. Used to set initial values.
        /// </summary>
        /// <param name="item">Owner entity.</param>
        public virtual void Init(GameEntity item) { }

        /// <summary>
        /// Abstract method called in owners <b>Update</b> method. Called once per frame.
        /// </summary>
        /// <param name="item">Owner entity.</param>
        public abstract void BehaviorUpdate(GameEntity item);
    }
}
