﻿using UnityEngine;
using Entities;
using Misc.UnityCursorControl;

namespace Behaviors
{   
    /// <summary>
    /// Rotates the owner based on the user input.
    /// </summary>
    public class RotateItemBehaviour : AbstractBehaviour
    {
        public static new string NAME = "RotateItemBehaviour";

        public float speedMultiplier = 20f;

        private bool rotating = false;

        private Vector3 lastCursorPosition;

        public bool Rotating { get => rotating; }

        override public void BehaviorUpdate(GameEntity item)
        {
            float horizontalAxis = Input.GetAxis("Mouse X");

            if (Input.GetMouseButton(1))
            {
                if (!Rotating) lastCursorPosition = CursorControl.GetGlobalCursorPos();
                //Cursor.visible = false;
                
                rotating = true;

                if (horizontalAxis != 0)
                {
                    item.transform.Rotate(0, -horizontalAxis * Time.deltaTime * speedMultiplier, 0);
                }
            }
            else if(Rotating)
            {
                CursorControl.SetGlobalCursorPos(lastCursorPosition);
                //Cursor.visible = true;

                rotating = false;               
            }
        }
    }
}