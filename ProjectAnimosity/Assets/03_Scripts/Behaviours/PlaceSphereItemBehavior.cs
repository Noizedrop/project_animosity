﻿using UnityEngine;

namespace Behaviors
{
    /// <summary>
    /// Placeable behavior with sphere border. A user defined number of points inside the sphere will be sampled with a ray-cast.
    /// If all sample results are inside the defined parameters the item will be marked as placeable.
    /// </summary>
    public class PlaceSphereItemBehavior : AbstractPlaceItemBehaviour
    {
        public static new string NAME = "PlaceSphereItemBehaviour";

        /// <summary>
        /// Angle percentage of the sample circle area. 90 for quarter circle, 180 for half circle, 360 for full circle etc.
        /// </summary>
        [Tooltip("Angle percentage of the sphere area. 180 for half circle, 360 for full circle.")]
        [Range(1f, 360f)]
        public int sampleArea = 360;

        /// <summary>
        /// Radius of the placeable sphere border.
        /// </summary>  
        [Tooltip("Radius of the sphere border.")]
        public float placableRadius = 5f;

        /// <summary>
        /// Distance scalar between samples. The higher the number the more rays will sample the terrain fragment (sample rays will be closer together).
        /// </summary>
        [Tooltip("Divider of the distance between samples.\nThe higher the number the more rays will sample the sample fragment and rays will be closer together.")]
        public float samplesDistanceScale = 10f;

        private void OnValidate()
        {
            if (placableRadius <= 0) placableRadius = 1;
            if (samplesCount <= 0) samplesCount = 1;
            if (samplesDistanceScale <= 0) samplesDistanceScale = 1f;
            if (maxHorizontalDifference <= 0) maxHorizontalDifference = 1f;

            if (samplesCount > placableRadius) placableRadius = samplesCount;
        }


        /// <summary>
        /// Samples the terrain points on the items placeable border defined by the <b>placableRadius</b> variable and checks for the highest terrain point.
        /// </summary>
        /// <param name="centerPoint">Items center point around which border is checked.</param>
        /// <returns>A Vecto4 value that contains the center point (XYZ) with the highest Y value on the border and placeable flag (W).</returns>       
        override protected Vector4 SampleBorderAroundCenterPoint(Vector3 centerHitPoint)
        {
            //TODO - ORGANIZE BETTER - REFACTOR INTO MULTIPLE METHODS

            if (DEBUG_IS_DEV)
            {
                float itemAreaRadius = (float)placableRadius;
                Vector3 centerHitPointTop = new Vector3(centerHitPoint.x, centerHitPoint.y + itemAreaRadius, centerHitPoint.z);
                Debug.DrawLine(centerHitPointTop, centerHitPoint, Color.blue);
            }

            float highestY = centerHitPoint.y;
            float isPlacable = 1f;

            float distanceBetweenSamples = (float)sampleArea / samplesCount / samplesDistanceScale;
            for (float angleAroundY = 0f; angleAroundY < sampleArea; angleAroundY += distanceBetweenSamples)
            {
                Vector3 borderPoint = new Vector3(centerHitPoint.x + placableRadius, centerHitPoint.y, centerHitPoint.z);

                Quaternion rotationMatrix = Quaternion.Euler(0, angleAroundY, 0);
                Vector3 samplePoint = rotationMatrix * (borderPoint - centerHitPoint) + centerHitPoint;

                samplePoint.y = Screen.height;

                Ray sampleRay = new Ray(samplePoint, sampleRayDirection);

                bool sampleRayHitResult = Physics.Raycast(sampleRay, out RaycastHit sampleHit, Mathf.Infinity, layerIndex);
                if (!sampleRayHitResult)
                {
                    if (DEBUG_IS_DEV) Debug.DrawLine(samplePoint, new Vector3(samplePoint.x, centerHitPoint.y, samplePoint.z), Color.red);
                    isPlacable = 0f;
                    continue;
                }

                Vector3 sampleHitPoint = sampleHit.point;

                if (DEBUG_IS_DEV) Debug.DrawLine(samplePoint, sampleHitPoint, Color.yellow);

                if (isPlacable != 0) isPlacable = IsSampleHitPointPlacable(sampleHit, centerHitPoint.y) ? 1f : 0f;

                float hitY = sampleHitPoint.y;
                if (hitY > highestY) highestY = hitY;
            }

            Vector4 sampleResult = new Vector4(centerHitPoint.x, highestY, centerHitPoint.z, isPlacable);
            return sampleResult;
        }

        /// <summary>
        /// Samples terrain points between the item's center and border are placeable. If all the sampled points are inside defined parameters then the item is placeable.
        /// </summary>
        /// <param name="centerHitPoint">The point a the items center.</param>
        /// <returns><b>True</b> if all the points are placeable else <b>False</b>.</returns>
        override protected Vector4 SamplePointsBetweenBorderAndCenter(Vector3 centerHitPoint)
        {
            //TODO - ORGANIZE BETTER - REFACTOR INTO MULTIPLE METHODS

            float highestY = centerHitPoint.y;
            float isPlacable = 1f;

            for (float sampleIndex = 1f; sampleIndex < samplesCount; sampleIndex++)
            {
                float distanceBetweenSamples = sampleArea / (samplesCount - sampleIndex) / samplesDistanceScale;

                for (float angleAroundY = 0f; angleAroundY < sampleArea; angleAroundY += distanceBetweenSamples)
                {
                    Vector3 borderPoint = new Vector3(centerHitPoint.x + placableRadius, centerHitPoint.y, centerHitPoint.z);

                    float angleAroundZ = 90f * (sampleIndex / samplesCount);
                    Quaternion rotationMatrix = Quaternion.Euler(0, angleAroundY, angleAroundZ);

                    Vector3 samplePoint = rotationMatrix * (borderPoint - centerHitPoint) + centerHitPoint;

                    Ray sampleRay = new Ray(samplePoint, sampleRayDirection);
                    bool sampleRayHitResult = Physics.Raycast(sampleRay, out RaycastHit sampleHit, Mathf.Infinity, layerIndex);

                    if (!sampleRayHitResult)
                    {
                        if (DEBUG_IS_DEV) Debug.DrawLine(samplePoint, new Vector3(samplePoint.x, centerHitPoint.y, samplePoint.z), Color.red);
                        isPlacable = 0f;

                        continue;
                    }

                    Vector3 sampleHitPoint = sampleHit.point;

                    if (DEBUG_IS_DEV) Debug.DrawLine(samplePoint, sampleHitPoint, Color.blue);

                    if (isPlacable != 0f) isPlacable *= IsSampleHitPointPlacable(sampleHit, sampleHitPoint.y) ? 1f : 0f;

                    float hitY = sampleHitPoint.y;

                    if (hitY > highestY) highestY = hitY;
                }
            }

            Vector4 sampleResult = new Vector4(centerHitPoint.x, highestY, centerHitPoint.z, isPlacable);
            return sampleResult;
        }
    }
}