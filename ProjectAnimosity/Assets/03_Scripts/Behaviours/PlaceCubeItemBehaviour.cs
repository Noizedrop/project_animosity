﻿using UnityEngine;

namespace Behaviors
{
    /// <summary>
    /// Placeable behavior with a cube border. A user defined number of points inside the cube will be sampled with a ray-cast.
    /// If all sample results are inside the defined parameters the item will be marked as placeable.
    /// </summary>
    public class PlaceCubeItemBehaviour : AbstractPlaceItemBehaviour
    {
        public static new string NAME = "PlaceCubeItemBehaviour";

        /// <summary>
        /// Dimensions of cube bounds that contains the placeable item.
        /// </summary>
        [Tooltip("Dimensions of cube bounds that contains the placeable item.")]
        public Vector3 placableBoundaries = new Vector3(5f, 5f, 5f);

        private void OnValidate()
        {
            if (placableBoundaries.x <= 0.1f) placableBoundaries.x = 1f;
            if (placableBoundaries.y <= 0.1f) placableBoundaries.y = 1f;
            if (placableBoundaries.z <= 0.1f) placableBoundaries.z = 1f;
        }

        /// <summary>
        /// Gets the highest terrain point that item border is touching.
        /// </summary>    
        /// <param name="centerPoint">Point whose Y value is to be corrected.</param>
        /// <returns>A point with the corrected Y value.</returns>        
        override protected Vector4 SampleBorderAroundCenterPoint(Vector3 centerPoint)
        {
            Vector2 upperLeftPoint = new Vector2(centerPoint.x - placableBoundaries.x * 0.5f, centerPoint.z + placableBoundaries.z * 0.5f);
            Vector2 lowerLeftPoint = new Vector2(centerPoint.x - placableBoundaries.x * 0.5f, centerPoint.z - placableBoundaries.z * 0.5f);

            Vector4 leftBorderPoints = new Vector4(upperLeftPoint.x, upperLeftPoint.y, lowerLeftPoint.x, lowerLeftPoint.y);

            float highestY = centerPoint.y;

            Vector2 sampleBorderResult = SampleHorizontalBorder(leftBorderPoints, centerPoint.y);
            highestY = sampleBorderResult.y > highestY ? sampleBorderResult.y : highestY;
            bool isPlacable = sampleBorderResult.x > 0;

            sampleBorderResult = SampleVerticalBorder(leftBorderPoints, centerPoint.y);
            highestY = sampleBorderResult.y > highestY ? sampleBorderResult.y : highestY;
            isPlacable &= sampleBorderResult.x > 0;


            Vector4 sampleResult = new Vector4(centerPoint.x, highestY, centerPoint.z, isPlacable ? 1f : 0f);

            return sampleResult;
        }

        /// <summary>
        /// Returns the highest Y value along the U (width or X) lines. Top and bottom cube border if observers from XZ (top down) perspective.
        /// </summary>
        /// <param name="leftBorderPoints">The ZX upper point and lower point. UV border form 1,0 to 0,0 coordinate.</param>        
        /// <returns>The maximum terrain Y point.</returns>
        protected Vector2 SampleHorizontalBorder(Vector4 leftBorderPoints, float centerY)
        {
            float highestY = centerY;
            float topYPoint = highestY + Screen.height;
            bool isPlacable = true;

            Vector2 lowerBorderSample;
            Vector2 upperBorderSample;

            float XdistanceBetweenSamples = placableBoundaries.x / samplesCount;

            float XRightBorder = leftBorderPoints.z + placableBoundaries.x;
            for (float x = leftBorderPoints.z; x <= XRightBorder; x += XdistanceBetweenSamples)
            {
                Vector3 lowerBorderPoint = new Vector3(x, topYPoint, leftBorderPoints.w);
                lowerBorderSample = SampleBorderLine(lowerBorderPoint, centerY);

                Vector3 upperBorderPoint = new Vector3(x, topYPoint, leftBorderPoints.y);
                upperBorderSample = SampleBorderLine(upperBorderPoint, centerY);

                isPlacable &= (lowerBorderSample.x + upperBorderSample.x) >= 2f;

                float currentSampleHighestY = lowerBorderSample.y > upperBorderSample.y ? lowerBorderSample.y : upperBorderSample.y;
                highestY = currentSampleHighestY > highestY ? currentSampleHighestY : highestY;
            }

            Vector2 sampleResult = new Vector2(isPlacable ? 1f : 0f, highestY);

            return sampleResult;
        }

        /// <summary>
        /// Gets the highest Y point along the V(length or Z) coordinates lines. Left and right cube border if observers from XZ (top down) perspective.
        /// </summary>
        /// <param name="lowerLeftPoint">The ZX lower point. UV coordinate 0,0.</param>
        /// <returns>The maximum terrain Y point.</returns>
        protected Vector2 SampleVerticalBorder(Vector4 leftBorderPoints, float centerY)
        {
            float highestY = centerY;
            float topYPoint = highestY + Screen.height;
            bool isPlacable = false;

            Vector2 lowerBorderSample;
            Vector2 upperBorderSample;

            float ZdistanceBetweenSamples = placableBoundaries.z / samplesCount;

            float XRightBorder = leftBorderPoints.z + placableBoundaries.x;
            for (float z = leftBorderPoints.w + ZdistanceBetweenSamples; z < leftBorderPoints.y; z += ZdistanceBetweenSamples)
            {
                Vector3 leftBorderPoint = new Vector3(leftBorderPoints.z, topYPoint, z);
                lowerBorderSample = SampleBorderLine(leftBorderPoint, centerY);

                Vector3 rightBorderPoint = new Vector3(XRightBorder, topYPoint, z);
                upperBorderSample = SampleBorderLine(rightBorderPoint, centerY);

                isPlacable = (lowerBorderSample.x + upperBorderSample.x) > 0;

                float currentSampleHighestY = lowerBorderSample.y > upperBorderSample.y ? lowerBorderSample.y : upperBorderSample.y;
                highestY = currentSampleHighestY > highestY ? currentSampleHighestY : highestY;
            }

            Vector2 sampleResult = new Vector2(isPlacable ? 1f : 0f, highestY);

            return sampleResult;
        }

        /// <summary>
        /// Samples the border point and compares the item center's Y value against sample result. Returns the highest of the two.
        /// </summary>
        /// <param name="borderPoint">The border point to be sampled.</param>
        /// <param name="highestY">Current highest Y value.</param>
        /// <returns>The higher Y value.</returns>
        protected Vector2 SampleBorderLine(Vector3 borderPoint, float centerY)
        {
            Ray sampleRay = new Ray(borderPoint, sampleRayDirection);

            float highestY = centerY;
            bool isPlacable = false;

            bool sampleRayHitResult = Physics.Raycast(sampleRay, out RaycastHit sampleHit, Mathf.Infinity, layerIndex);

            if (sampleRayHitResult)
            {
                Vector3 sampleHitPoint = sampleHit.point;
                
                isPlacable = IsSampleHitPointPlacable(sampleHit, centerY);

                highestY = sampleHitPoint.y > highestY ? sampleHitPoint.y : highestY;

                if (DEBUG_IS_DEV) Debug.DrawLine(borderPoint, sampleHitPoint, Color.yellow);
            }

            return new Vector2(isPlacable ? 1f : 0f, highestY);
        }       

        /// <summary>
        /// Checks if all the sampled terrain points around the item center are placeable.
        /// </summary>
        /// <param name="centerHitPoint">The center ray-cast terrain point.</param>
        /// <returns><b>True</b> if all the points are placeable else <b>False</b>.</returns>
        override protected Vector4 SamplePointsBetweenBorderAndCenter(Vector3 centerHitPoint)
        {
            float highestY = centerHitPoint.y;
            bool isPlacable = true;

            Vector3 lowerLeftPoint = new Vector3(centerHitPoint.x - placableBoundaries.x * 0.5f, centerHitPoint.y, centerHitPoint.z - placableBoundaries.z * 0.5f);
            float boundaryHeight = placableBoundaries.y;

            float XdistanceBetweenSamples = placableBoundaries.x / samplesCount;
            float ZdistanceBetweenSamples = placableBoundaries.z / samplesCount;

            float XleftBoundary = lowerLeftPoint.x + placableBoundaries.x;
            float ZleftBoundary = lowerLeftPoint.z + placableBoundaries.z;

            //Vector3 distanceBetweenSamples = new Vector3(XdistanceBetweenSamples, 0f, ZdistanceBetweenSamples);

            for (float x = lowerLeftPoint.x + XdistanceBetweenSamples; x < XleftBoundary; x += XdistanceBetweenSamples)
            {
                for (float z = lowerLeftPoint.z + ZdistanceBetweenSamples; z < ZleftBoundary; z += ZdistanceBetweenSamples)
                {
                    Vector3 samplePoint = new Vector3(x, centerHitPoint.y + boundaryHeight, z);

                    isPlacable &= IsSamplePointPlacable(samplePoint, centerHitPoint.y);
                    highestY = centerHitPoint.y > highestY ? centerHitPoint.y : highestY;                   
                }
            }

            Vector4 sampleResult = new Vector4(centerHitPoint.x, highestY, centerHitPoint.z, isPlacable ? 1f : 0f);
            return sampleResult;        
        }

        /// <summary>
        /// Method samples the terrain's sample point and checks if it is placeable.
        /// </summary>
        /// <param name="samplePoint">Sample point in world coordinates.</param>
        /// <param name="centerY">The value of the item's center Y value.</param>
        /// <returns><b>True</b> if the point is placeable else <b>False</b>.</returns>
        protected bool IsSamplePointPlacable(Vector3 samplePoint, float centerY)
        {
            bool isPlacable = true;

            Ray sampleRay = new Ray(samplePoint, sampleRayDirection);

            bool sampleRayHitResult = Physics.Raycast(sampleRay, out RaycastHit sampleHit, Mathf.Infinity, layerIndex);

            if (sampleRayHitResult)
            {                
                isPlacable &= IsSampleHitPointPlacable(sampleHit, centerY);

                if (DEBUG_IS_DEV) Debug.DrawLine(samplePoint, sampleHit.point, Color.blue);                
            }
            else
            {
                isPlacable = false;

                if (DEBUG_IS_DEV) Debug.DrawLine(samplePoint, sampleHit.point, Color.red);                
            }

            return isPlacable;
        }
    }
}