﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingletonBehaviour<T> : MonoBehaviour where T : MonoBehaviour
{

    private static T s_instance;
    public bool isPersistent = true;

    public static T instance
    {
        get
        {
            return s_instance;
        }
    }

    protected virtual void Awake()
    {
        if (s_instance != null && s_instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            s_instance = this as T;
            if (isPersistent)
            {
                DontDestroyOnLoad(this.gameObject);
            }
        }
    }
}
