﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.VFX;

public class SpawnEffectVFX : MonoBehaviour
{
    public VisualEffect effect;

    void Update()
    {
        if (Input.GetKeyDown("space"))
        {
            Instantiate(effect, Random.insideUnitSphere * 5, Quaternion.identity);            
        }
    }
}
