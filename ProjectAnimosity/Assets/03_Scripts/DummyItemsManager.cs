﻿using UnityEngine;
using Behaviors;
using Entities;

/// <summary>
/// Dummy Manager Creates initializes an places a dummy placeable item. Debug use only.
/// Press 1 to instantiate sphere.
/// Press 2 to instantiate cube.
/// </summary>
public class DummyItemsManager : MonoBehaviour
{
    /// <summary>
    /// Sphere
    /// </summary>
    public GameObject spherePrefab;

    /// <summary>
    /// Cube
    /// </summary>
    public GameObject cubePrefab;

    private GameObject sphereInstance = null;
    private GameObject cubeInstance = null;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1) && sphereInstance == null)
        {
            if (cubeInstance != null) Destroy(cubeInstance);
            cubeInstance = null;

            sphereInstance = Instantiate(spherePrefab, Vector3.zero, spherePrefab.transform.rotation);
        }

        if (Input.GetKeyDown(KeyCode.Alpha2) && cubeInstance == null)
        {
            if (sphereInstance != null) Destroy(sphereInstance);
            sphereInstance = null;

            cubeInstance = Instantiate(cubePrefab, Vector3.zero, cubePrefab.transform.rotation);
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.cyan;

        if (sphereInstance != null)
        {
            GameEntity sphereEntity = sphereInstance.GetComponent<GameEntity>();

            PlaceSphereItemBehavior placeSphereItemBehaviour = sphereEntity.GetBehaviour<PlaceSphereItemBehavior>() as PlaceSphereItemBehavior;

            Gizmos.DrawWireSphere(sphereInstance.transform.position, placeSphereItemBehaviour.placableRadius);
        }

        if (cubeInstance != null)
        {
            GameEntity cubeEntity = cubeInstance.GetComponent<GameEntity>();

            PlaceCubeItemBehaviour placeCubeItemBehaviour = cubeEntity.GetBehaviour<PlaceCubeItemBehaviour>() as PlaceCubeItemBehaviour;
            Vector3 boundaries = placeCubeItemBehaviour.placableBoundaries;
            Vector3 centerOffset = new Vector3(0f, boundaries.y * 0.5f, 0f);

            Vector3 cubePosition = cubeInstance.transform.position + centerOffset;

            Gizmos.DrawWireCube(cubePosition, boundaries);
        }
    }
}
